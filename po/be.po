# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/secrets/issues\n"
"POT-Creation-Date: 2025-02-19 18:49+0000\n"
"PO-Revision-Date: 2025-02-23 02:22+0300\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: be\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);\n"
"X-Generator: Poedit 3.4.4\n"

#: data/gtk/add_attribute_dialog.ui:5
msgid "Add Attribute"
msgstr ""

#: data/gtk/add_attribute_dialog.ui:37
msgid "Key"
msgstr "Ключ"

#: data/gtk/add_attribute_dialog.ui:43
msgid "Value"
msgstr "Значэнне"

#: data/gtk/add_attribute_dialog.ui:49
msgid "Sensitive"
msgstr "Адчувальнасць"

#: data/gtk/add_attribute_dialog.ui:50
msgid "Contains sensitive data."
msgstr "Змяшчае адчувальную інфармацыю."

#: data/gtk/add_attribute_dialog.ui:57 gsecrets/entry_page.py:297
#: gsecrets/entry_page.py:454
msgid "_Add"
msgstr ""

#: data/gtk/attachment_entry_row.ui:9
msgid "Download Attachment"
msgstr ""

#: data/gtk/attachment_entry_row.ui:20
msgid "Remove Attachment"
msgstr ""

#. Dialog title which informs the user about unsaved changes.
#: data/gtk/attachment_warning_dialog.ui:4
msgid "Safety Info"
msgstr "Бяспечная інфармацыя"

#. Dialog subtitle which informs the user about unsaved changes more detailed.
#: data/gtk/attachment_warning_dialog.ui:5
msgid ""
"It is possible that external applications will create unencrypted hidden or "
"temporary copies of this attachment file! Please proceed with caution."
msgstr ""

#: data/gtk/attachment_warning_dialog.ui:9
msgid "_Back"
msgstr ""

#: data/gtk/attachment_warning_dialog.ui:10
msgid "_Proceed"
msgstr ""

#: data/gtk/attribute_entry_row.ui:8
#: gsecrets/widgets/protected_attribute_entry_row.py:28
msgid "Copy Attribute"
msgstr ""

#: data/gtk/attribute_entry_row.ui:19
#: gsecrets/widgets/protected_attribute_entry_row.py:34
msgid "Remove Attribute"
msgstr ""

#: data/gtk/browsing_panel.ui:23
msgid "Add elements with the ➕ button"
msgstr ""

#: data/gtk/browsing_panel.ui:24
msgid "Empty Group"
msgstr "Пустая група"

#: data/gtk/browsing_panel.ui:30
msgid "No Search Results"
msgstr "Няма вынікаў пошуку"

#: data/gtk/color_entry_row.ui:4
msgid "Color"
msgstr "Колер"

#. Headerbar subtitle in keepass safe creation routine.
#. NOTE: Filechooser title for creating a new keepass safe kdbx file
#: data/gtk/create_database.ui:11 gsecrets/widgets/window.py:238
msgid "Create Safe"
msgstr ""

#: data/gtk/create_database.ui:17
msgid "Back"
msgstr "Назад"

#: data/gtk/create_database.ui:22 data/gtk/locked_headerbar.ui:20
#: data/gtk/unlocked_headerbar.ui:40
msgid "Main Menu"
msgstr "Асноўнае меню"

#: data/gtk/create_database.ui:44
msgid "Protect your safe"
msgstr ""

#: data/gtk/create_database.ui:57 data/gtk/credentials_group.ui:23
#: data/gtk/references_dialog.ui:177
msgid "_Password"
msgstr ""

#: data/gtk/create_database.ui:64
msgid "_Confirm Password"
msgstr ""

#: data/gtk/create_database.ui:84 data/gtk/unlock_database.ui:45
msgid "Additional Credentials"
msgstr ""

#: data/gtk/create_database.ui:90
msgid "C_reate"
msgstr ""

#: data/gtk/create_database.ui:115
msgid "Safe Successfully Created"
msgstr ""

#: data/gtk/create_database.ui:118
msgid "_Open Safe"
msgstr ""

#: data/gtk/create_database.ui:143 data/gtk/locked_headerbar.ui:38
#: data/gtk/unlocked_headerbar.ui:103
msgid "_Preferences"
msgstr ""

#: data/gtk/create_database.ui:147 data/gtk/locked_headerbar.ui:42
#: data/gtk/unlocked_headerbar.ui:107
msgid "_Keyboard Shortcuts"
msgstr ""

#. "Secrets" is the application name, do not translate
#: data/gtk/create_database.ui:151 data/gtk/locked_headerbar.ui:46
#: data/gtk/unlocked_headerbar.ui:111
msgid "_About Secrets"
msgstr ""

#: data/gtk/credentials_group.ui:6 data/gtk/references_dialog.ui:173
msgid "_Username"
msgstr ""

#: data/gtk/credentials_group.ui:11 data/gtk/credentials_group.ui:47
#: data/gtk/entry_page.ui:93 data/gtk/entry_page.ui:131
#: data/gtk/history_row.ui:28 data/gtk/references_dialog.ui:345
msgid "Copy"
msgstr "Скапіяваць"

#: data/gtk/credentials_group.ui:32 data/gtk/database_settings_dialog.ui:31
#: gsecrets/create_database.py:40
msgid "Generate New Password"
msgstr "Згенераваць новы пароль"

#: data/gtk/database_settings_dialog.ui:5
msgid "Safe Settings"
msgstr "Бяспечныя налады"

#: data/gtk/database_settings_dialog.ui:9
msgid "Authentication"
msgstr ""

#: data/gtk/database_settings_dialog.ui:15
msgid "Current Password"
msgstr ""

#: data/gtk/database_settings_dialog.ui:25
msgid "New Password"
msgstr "Новы пароль"

#: data/gtk/database_settings_dialog.ui:46
msgid "Confirm New Password"
msgstr "Пацвердзіць новы пароль"

#: data/gtk/database_settings_dialog.ui:60
msgid "New Additional Credentials"
msgstr ""

#: data/gtk/database_settings_dialog.ui:75
#: gsecrets/widgets/database_settings_dialog.py:196
#: gsecrets/widgets/database_settings_dialog.py:258
msgid "_Apply Changes"
msgstr ""

#: data/gtk/database_settings_dialog.ui:93 data/gtk/settings_dialog.ui:8
#: data/gtk/settings_dialog.ui:11
msgid "General"
msgstr ""

#: data/gtk/database_settings_dialog.ui:97
msgid "Database"
msgstr "База даных"

#: data/gtk/database_settings_dialog.ui:100
msgid "Name"
msgstr "Імя"

#: data/gtk/database_settings_dialog.ui:108
msgid "Description"
msgstr "Апісанне"

#: data/gtk/database_settings_dialog.ui:116
msgid "Default User"
msgstr "Карыстальнік па змаўчанні"

#: data/gtk/database_settings_dialog.ui:127
msgid "Details"
msgstr "Падрабязнасці"

#: data/gtk/database_settings_dialog.ui:130
msgid "Path"
msgstr "Шлях"

#: data/gtk/database_settings_dialog.ui:139
msgid "File Size"
msgstr "Памер файла"

#. KeePass is a proper noun
#: data/gtk/database_settings_dialog.ui:148
msgid "KeePass Version"
msgstr "Версія KeePass"

#: data/gtk/database_settings_dialog.ui:157
msgid "Creation Date"
msgstr "Дата стварэння"

#: data/gtk/database_settings_dialog.ui:165
msgid "Encryption Algorithm"
msgstr "Алгарытм шыфравання"

#: data/gtk/database_settings_dialog.ui:173
msgid "Derivation Algorithm"
msgstr ""

#. Statistics
#: data/gtk/database_settings_dialog.ui:183
msgid "Stats"
msgstr ""

#: data/gtk/database_settings_dialog.ui:186
msgid "Number of Entries"
msgstr ""

#: data/gtk/database_settings_dialog.ui:195
msgid "Number of Groups"
msgstr ""

#: data/gtk/database_settings_dialog.ui:204
msgid "Passwords"
msgstr "Паролі"

#: data/gtk/entry_page.ui:55
msgid "Entry Menu"
msgstr ""

#: data/gtk/entry_page.ui:69 data/gtk/group_page.ui:26
#: data/gtk/references_dialog.ui:169
msgid "_Title"
msgstr ""

#: data/gtk/entry_page.ui:75
msgid "U_RL"
msgstr ""

#: data/gtk/entry_page.ui:81
msgid "Open Address"
msgstr "Адкрыць адрас"

#: data/gtk/entry_page.ui:109
msgid "One-Time Password"
msgstr "Аднаразовы пароль"

#: data/gtk/entry_page.ui:121
msgid "Remaining Time"
msgstr "Засталося часу"

#: data/gtk/entry_page.ui:143
msgid "One-_Time Password Secret"
msgstr ""

#. OTP is a proper name
#: data/gtk/entry_page.ui:149
msgid "Could not Generate OTP Token"
msgstr "Не ўдалося згенераваць OTP токен"

#: data/gtk/entry_page.ui:157 data/gtk/group_page.ui:34
#: data/gtk/notes_dialog.ui:6 data/gtk/references_dialog.ui:141
msgid "Notes"
msgstr "Нататкі"

#: data/gtk/entry_page.ui:161 data/gtk/group_page.ui:38
msgid "Edit in a Window"
msgstr "Змяніць у акне"

#: data/gtk/entry_page.ui:199
msgid "Icon"
msgstr ""

#: data/gtk/entry_page.ui:219
msgid "Tags"
msgstr ""

#: data/gtk/entry_page.ui:224 gsecrets/entry_page.py:295
msgid "Add New Tag"
msgstr ""

#: data/gtk/entry_page.ui:241
msgid "Attachments"
msgstr ""

#: data/gtk/entry_page.ui:245
msgid "_Add Attachment"
msgstr ""

#: data/gtk/entry_page.ui:254
msgid "Attributes"
msgstr ""

#: data/gtk/entry_page.ui:259
msgid "Add Attr_ibute"
msgstr ""

#: data/gtk/entry_page.ui:267
msgid "Expiration Date"
msgstr ""

#: data/gtk/entry_page.ui:280
msgid "_Show More"
msgstr ""

#: data/gtk/entry_page.ui:294
msgid "D_uplicate"
msgstr ""

#: data/gtk/entry_page.ui:298
msgid "_References"
msgstr ""

#: data/gtk/entry_page.ui:302 data/gtk/group_page.ui:75
msgid "_Properties"
msgstr ""

#: data/gtk/entry_page.ui:308
msgid "_Save in History"
msgstr ""

#: data/gtk/entry_page.ui:312
msgid "Show _History"
msgstr ""

#: data/gtk/entry_page.ui:318 data/gtk/group_page.ui:81
#: gsecrets/selection_manager.py:112
msgid "_Delete"
msgstr ""

#: data/gtk/entry_page_tag.ui:21
msgid "Remove"
msgstr "Выдаліць"

#: data/gtk/entry_row.ui:38
msgid "Select Entry"
msgstr ""

#: data/gtk/entry_row.ui:82
msgid "Copy One-Time Password"
msgstr ""

#: data/gtk/entry_row.ui:93
msgid "Copy Username"
msgstr ""

#: data/gtk/entry_row.ui:103
msgid "Copy Password"
msgstr ""

#: data/gtk/expiration_date_row.ui:8 gsecrets/widgets/expiration_date_row.py:82
msgid "Expiration Date not Set"
msgstr ""

#: data/gtk/expiration_date_row.ui:22
msgid "Edit Expiration Date"
msgstr ""

#: data/gtk/expiration_date_row.ui:30
msgid "Remove Expiration Date"
msgstr ""

#: data/gtk/expiration_date_row.ui:63
msgid "_Set Expiration Date"
msgstr ""

#: data/gtk/group_page.ui:14
msgid "Group Menu"
msgstr ""

#: data/gtk/group_row.ui:38
msgid "Select Group"
msgstr "Выбраць групу"

#: data/gtk/group_row.ui:68
msgid "Edit"
msgstr "Змяніць"

#: data/gtk/help-overlay.ui:11
msgctxt "Shortcut window description"
msgid "Application"
msgstr ""

#: data/gtk/help-overlay.ui:14
msgctxt "Shortcut window description"
msgid "Create Safe"
msgstr ""

#: data/gtk/help-overlay.ui:20
msgctxt "Shortcut window description"
msgid "Go Back"
msgstr ""

#: data/gtk/help-overlay.ui:26
msgctxt "Shortcut window description"
msgid "Open Safe"
msgstr ""

#: data/gtk/help-overlay.ui:32
msgctxt "Shortcut window description"
msgid "Open Preferences"
msgstr ""

#: data/gtk/help-overlay.ui:38
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr ""

#: data/gtk/help-overlay.ui:44
msgctxt "Shortcut window description"
msgid "Close Window"
msgstr ""

#: data/gtk/help-overlay.ui:50
msgctxt "Shortcut window description"
msgid "New Window"
msgstr ""

#: data/gtk/help-overlay.ui:56
msgctxt "Shortcut window description"
msgid "Quit"
msgstr ""

#: data/gtk/help-overlay.ui:64
msgctxt "Shortcut window description"
msgid "Safe"
msgstr ""

#: data/gtk/help-overlay.ui:67
msgctxt "Shortcut window description"
msgid "Lock Safe"
msgstr ""

#: data/gtk/help-overlay.ui:73
msgctxt "Shortcut window description"
msgid "Save Safe"
msgstr ""

#: data/gtk/help-overlay.ui:79
msgctxt "Shortcut window description"
msgid "Open Search"
msgstr ""

#: data/gtk/help-overlay.ui:85
msgctxt "Shortcut window description"
msgid "Create Entry"
msgstr ""

#: data/gtk/help-overlay.ui:91
msgctxt "Shortcut window description"
msgid "Create Group"
msgstr ""

#: data/gtk/help-overlay.ui:99
msgctxt "Shortcut window description"
msgid "Entry"
msgstr ""

#: data/gtk/help-overlay.ui:102
msgctxt "Shortcut window description"
msgid "Copy Password"
msgstr ""

#: data/gtk/help-overlay.ui:108
msgctxt "Shortcut window description"
msgid "Copy Address"
msgstr ""

#: data/gtk/help-overlay.ui:114
msgctxt "Shortcut window description"
msgid "Copy Username"
msgstr ""

#: data/gtk/help-overlay.ui:120
msgctxt "Shortcut window description"
msgid "Copy One-Time Password"
msgstr ""

#: data/gtk/help-overlay.ui:126
msgctxt "Shortcut window description"
msgid "Show History"
msgstr ""

#: data/gtk/help-overlay.ui:132
msgctxt "Shortcut window description"
msgid "Save in History"
msgstr ""

#: data/gtk/history_row.ui:18 gsecrets/widgets/history_row.py:47
msgid "Show Text"
msgstr "Паказаць тэкст"

#: data/gtk/history_row.ui:39
msgid "Remove from History"
msgstr "Выдаліць з гісторыі"

#: data/gtk/history_window.ui:8
msgid "Password History"
msgstr "Гісторыя пароляў"

#: data/gtk/locked_headerbar.ui:14
msgid "_Open"
msgstr ""

#: data/gtk/locked_headerbar.ui:32 data/gtk/unlocked_headerbar.ui:89
#: data/gtk/welcome_page.ui:18
msgid "_New Safe"
msgstr ""

#: data/gtk/notes_dialog.ui:20
msgid "_Copy"
msgstr ""

#. Button tooltip in headerbar to open search page
#: data/gtk/notes_dialog.ui:33 data/gtk/notes_dialog.ui:58
#: data/gtk/unlocked_headerbar.ui:26
msgid "Search"
msgstr "Пошук"

#: data/gtk/password_generator_popover.ui:23 data/gtk/references_dialog.ui:117
#: data/gtk/settings_dialog.ui:53 data/gtk/unlock_database.ui:37
msgid "Password"
msgstr "Пароль"

#: data/gtk/password_generator_popover.ui:32
msgid "Length"
msgstr "Даўжыня"

#: data/gtk/password_generator_popover.ui:50
msgid "Characters"
msgstr "Знакі"

#: data/gtk/password_generator_popover.ui:94
#: gsecrets/provider/pkcs11_provider.py:213
msgid "Passphrase"
msgstr ""

#: data/gtk/password_generator_popover.ui:99
msgid "Words"
msgstr "Словы"

#. Button to generate a password
#: data/gtk/password_generator_popover.ui:126
#: gsecrets/provider/file_provider.py:196
msgid "_Generate"
msgstr ""

#: data/gtk/properties_dialog.ui:6
msgid "Properties"
msgstr "Уласцівасці"

#: data/gtk/properties_dialog.ui:26 data/gtk/references_dialog.ui:153
msgid "UUID"
msgstr ""

#: data/gtk/properties_dialog.ui:35
msgid "Accessed"
msgstr ""

#: data/gtk/properties_dialog.ui:44
msgid "Modified"
msgstr ""

#: data/gtk/properties_dialog.ui:53
msgid "Created"
msgstr ""

#: data/gtk/references_dialog.ui:15
msgid "Code"
msgstr "Код"

#: data/gtk/references_dialog.ui:27 data/gtk/references_dialog.ui:241
#: data/gtk/references_dialog.ui:337
msgid "Property"
msgstr "Уласцівасць"

#: data/gtk/references_dialog.ui:48
msgid "Title"
msgstr "Загаловак"

#: data/gtk/references_dialog.ui:69
msgid "Username"
msgstr ""

#: data/gtk/references_dialog.ui:129
msgid "URL"
msgstr ""

#: data/gtk/references_dialog.ui:181
msgid "_URL"
msgstr ""

#: data/gtk/references_dialog.ui:185
msgid "_Notes"
msgstr ""

#: data/gtk/references_dialog.ui:191
msgid "References"
msgstr ""

#: data/gtk/references_dialog.ui:212
msgid "Syntax"
msgstr "Cінтаксіс"

#: data/gtk/references_dialog.ui:221
msgid "The placeholder syntax for field references is the following:"
msgstr ""

#: data/gtk/references_dialog.ui:240 data/gtk/references_dialog.ui:268
msgid "Show Field Codes"
msgstr ""

#: data/gtk/references_dialog.ui:269
msgid "Identifier"
msgstr ""

#: data/gtk/references_dialog.ui:326
msgid "Reference"
msgstr ""

#: data/gtk/references_dialog.ui:365
msgid ""
"Each entry has a unique identifier called UUID. It can be found in the "
"properties of the entry."
msgstr ""

#. Dialog title which informs the user about unsaved changes.
#: data/gtk/save_dialog.ui:4
msgid "Unsaved Changes"
msgstr ""

#. Dialog subtitle which informs the user about unsaved changes more detailed.
#: data/gtk/save_dialog.ui:5
msgid "Do you want to write all changes to the safe?"
msgstr ""

#. _Discard all the changes which the user have made to his keepass safe
#: data/gtk/save_dialog.ui:10 gsecrets/widgets/quit_conflict_dialog.py:24
#: gsecrets/widgets/window.py:419
msgid "_Quit Without Saving"
msgstr ""

#. _Cancel exiting the program
#: data/gtk/save_dialog.ui:11 gsecrets/widgets/window.py:420
msgid "_Don't Quit"
msgstr ""

#. _Save all the changes which the user have made to his keepass safe
#: data/gtk/save_dialog.ui:12
msgid "_Save and Quit"
msgstr ""

#: data/gtk/settings_dialog.ui:14
msgid "_Hide First Start Screen"
msgstr ""

#: data/gtk/settings_dialog.ui:15
msgid "Reopen last opened safe."
msgstr "Пераадкрыць апошні адкрыты файл."

#. TRANSLATORS Safe as in strongbox.
#. NOTE: Safe as in strongbox
#: data/gtk/settings_dialog.ui:24 gsecrets/widgets/window.py:147
#: gsecrets/widgets/window.py:230 gsecrets/widgets/window.py:242
msgid "Safe"
msgstr ""

#: data/gtk/settings_dialog.ui:27
msgid "_Save Automatically"
msgstr ""

#: data/gtk/settings_dialog.ui:28
msgid "Save every change automatically."
msgstr ""

#: data/gtk/settings_dialog.ui:37 gsecrets/provider/pkcs11_provider.py:216
msgid "Unlock"
msgstr "Разблакаваць"

#. A composite key is a authentication method where the user needs both password and keyfile for unlocking his safe
#: data/gtk/settings_dialog.ui:40
msgid "Remember Composite _Key"
msgstr ""

#. A composite key is a authentication method where the user needs both password and keyfile for unlocking his safe
#: data/gtk/settings_dialog.ui:41
msgid "Remember last used composite key."
msgstr "Запомніць апошні скарыстаны кампазітны ключ"

#: data/gtk/settings_dialog.ui:56
msgid "Password Generation"
msgstr "Генерацыя пароля"

#: data/gtk/settings_dialog.ui:59
msgid "Password Length"
msgstr "Даўжыня пароля"

#: data/gtk/settings_dialog.ui:60
msgid "Number of characters when generating a password."
msgstr ""

#: data/gtk/settings_dialog.ui:74
msgid "_Uppercase Characters"
msgstr ""

#: data/gtk/settings_dialog.ui:75
msgid "Use uppercase characters A-Z when generating a password."
msgstr ""

#: data/gtk/settings_dialog.ui:82
msgid "_Lowercase Characters"
msgstr ""

#: data/gtk/settings_dialog.ui:83
msgid "Use lowercase characters a-z when generating a password."
msgstr ""

#: data/gtk/settings_dialog.ui:90
msgid "_Numeric Characters"
msgstr ""

#: data/gtk/settings_dialog.ui:91
msgid "Use numeric characters when generating a password."
msgstr ""

#: data/gtk/settings_dialog.ui:98
msgid "_Special Characters"
msgstr ""

#. ASCII is a proper name
#: data/gtk/settings_dialog.ui:99
msgid "Use non-alphanumeric ASCII symbols when generating a password."
msgstr ""

#: data/gtk/settings_dialog.ui:108
msgid "Passphrase Generation"
msgstr ""

#: data/gtk/settings_dialog.ui:111
msgid "Passphrase Length"
msgstr ""

#: data/gtk/settings_dialog.ui:112
#: data/org.gnome.World.Secrets.gschema.xml.in:103
msgid "Number of words to use when generating a passphrase."
msgstr ""

#: data/gtk/settings_dialog.ui:126
msgid "Separator"
msgstr "Раздзяляльнік"

#: data/gtk/settings_dialog.ui:136 data/gtk/settings_dialog.ui:139
msgid "Security"
msgstr "Бяспека"

#: data/gtk/settings_dialog.ui:142
msgid "Safe Lock Timeout"
msgstr ""

#: data/gtk/settings_dialog.ui:143
msgid "Lock safe on idle after X minutes."
msgstr ""

#: data/gtk/settings_dialog.ui:157
msgid "Lock on _Session Lock"
msgstr ""

#: data/gtk/settings_dialog.ui:158
#: data/org.gnome.World.Secrets.gschema.xml.in:13
msgid "Automatically lock when the session is locked."
msgstr ""

#: data/gtk/settings_dialog.ui:165
msgid "Clear Clipboard"
msgstr ""

#: data/gtk/settings_dialog.ui:166
msgid "Clear clipboard after X seconds."
msgstr ""

#: data/gtk/settings_dialog.ui:180
msgid "Clear Recent List"
msgstr ""

#: data/gtk/settings_dialog.ui:181
msgid "Clear the recently opened safes list."
msgstr ""

#: data/gtk/settings_dialog.ui:188
msgid "Clear"
msgstr ""

#: data/gtk/unlock_database.ui:20
msgid "Unlock Safe"
msgstr ""

#: data/gtk/unlock_database.ui:51 gsecrets/provider/pkcs11_provider.py:218
msgid "_Unlock"
msgstr ""

#: data/gtk/unlocked_database.ui:18
msgid "Browser"
msgstr ""

#: data/gtk/unlocked_database.ui:29 gsecrets/unlocked_database.py:698
msgid "Select Items"
msgstr "Выбраць элементы"

#: data/gtk/unlocked_database.ui:35 data/gtk/unlocked_headerbar.ui:11
msgid "Go Back"
msgstr "Вярнуцца"

#: data/gtk/unlocked_database.ui:41 gsecrets/entry_page.py:296
#: gsecrets/provider/pkcs11_provider.py:217 gsecrets/selection_manager.py:111
#: gsecrets/widgets/saving_conflict_dialog.py:31
msgid "_Cancel"
msgstr ""

#: data/gtk/unlocked_database.ui:54 data/gtk/unlocked_database.ui:60
msgid "Search Safe"
msgstr ""

#: data/gtk/unlocked_database.ui:73
msgid "_Clear"
msgstr ""

#. Button tooltip in selection mode to delete every selected entry/group
#: data/gtk/unlocked_database.ui:83
msgid "Delete Selection"
msgstr ""

#. Button tooltip in selection mode to move every selected entry/group to another place
#: data/gtk/unlocked_database.ui:93
msgid "Move Selection"
msgstr ""

#: data/gtk/unlocked_database.ui:100
msgid "Paste Selection"
msgstr ""

#: data/gtk/unlocked_database.ui:125
msgid "Select an Entry"
msgstr ""

#: data/gtk/unlocked_headerbar.ui:18
msgid "New Entry or Group"
msgstr ""

#. Button tooltip in headerbar to switch to selection mode where the user can tick password entries and groups
#: data/gtk/unlocked_headerbar.ui:33
msgid "Selection Mode"
msgstr ""

#. s
#: data/gtk/unlocked_headerbar.ui:54
#: gsecrets/widgets/saving_conflict_dialog.py:34
msgid "_Save"
msgstr ""

#. s
#: data/gtk/unlocked_headerbar.ui:58
msgid "_Lock"
msgstr ""

#: data/gtk/unlocked_headerbar.ui:64
msgid "So_rting"
msgstr ""

#. This is an alphabetical order for entries
#: data/gtk/unlocked_headerbar.ui:66
msgid "_A-Z"
msgstr ""

#. This is an alphabetical order for entries
#: data/gtk/unlocked_headerbar.ui:71
msgid "_Z-A"
msgstr ""

#: data/gtk/unlocked_headerbar.ui:76
msgid "_Newest First"
msgstr ""

#: data/gtk/unlocked_headerbar.ui:81
msgid "_Oldest First"
msgstr ""

#: data/gtk/unlocked_headerbar.ui:93
msgid "New _Window"
msgstr ""

#: data/gtk/unlocked_headerbar.ui:97
msgid "Sa_fe Settings"
msgstr ""

#: data/gtk/unlocked_headerbar.ui:119
msgid "New _Entry"
msgstr ""

#: data/gtk/unlocked_headerbar.ui:123
msgid "New _Group"
msgstr ""

#. The umbrella sentence of the application on the first start screen
#: data/gtk/welcome_page.ui:11
msgid "Secure Secrets"
msgstr ""

#. The subtitle of the umbrella sentence in the first start screen. This is a sentence which gives the user a starting point what he can do if he opens the application for the first time.
#: data/gtk/welcome_page.ui:12
msgid "Create or import a new Keepass safe"
msgstr ""

#. Translators: Secrets is the app name, do not translate
#: data/gtk/window.ui:4 data/org.gnome.World.Secrets.desktop.in.in:4
msgid "Secrets"
msgstr ""

#: data/org.gnome.World.Secrets.desktop.in.in:5
msgid "A password manager for GNOME"
msgstr ""

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.World.Secrets.desktop.in.in:14
msgid "keepass;encrypt;secure;"
msgstr ""

#: data/org.gnome.World.Secrets.desktop.in.in:21
msgid "New Window"
msgstr "Новае акно"

#: data/org.gnome.World.Secrets.gschema.xml.in:12
msgid "Lock on session lock"
msgstr ""

#: data/org.gnome.World.Secrets.gschema.xml.in:17
msgid "Reopen last opened database"
msgstr ""

#: data/org.gnome.World.Secrets.gschema.xml.in:18
msgid ""
"Automatically open the unlock screen of the last opened database, otherwise "
"show the welcome screen."
msgstr ""

#: data/org.gnome.World.Secrets.gschema.xml.in:22
msgid "Save every change automatically"
msgstr ""

#: data/org.gnome.World.Secrets.gschema.xml.in:23
msgid ""
"Save every change you made instantly into the database. Please note that you "
"cannot revert changes if Autosave is enabled."
msgstr ""

#: data/org.gnome.World.Secrets.gschema.xml.in:27
msgid "Lock database after X minutes"
msgstr ""

#: data/org.gnome.World.Secrets.gschema.xml.in:28
msgid ""
"Automatically lock your database after a given amount of minutes to improve "
"the security."
msgstr ""

#: data/org.gnome.World.Secrets.gschema.xml.in:32
msgid "Clear clipboard after X seconds"
msgstr ""

#: data/org.gnome.World.Secrets.gschema.xml.in:33
msgid "After copying the password clear the clipboard for security reasons."
msgstr ""

#: data/org.gnome.World.Secrets.gschema.xml.in:37
msgid "Window size"
msgstr "Велічыня акна"

#: data/org.gnome.World.Secrets.gschema.xml.in:38
msgid "Remember the window size."
msgstr "Запомніць велічыню акна."

#: data/org.gnome.World.Secrets.gschema.xml.in:42
msgid "Last opened databases"
msgstr "Апошнія адкрытыя базы даных"

#: data/org.gnome.World.Secrets.gschema.xml.in:43
msgid "A list of the last opened databases."
msgstr "Спіс апошніх адкрытых баз даных."

#: data/org.gnome.World.Secrets.gschema.xml.in:47
msgid "Last opened database"
msgstr "Апошняя адкрытая база даных"

#: data/org.gnome.World.Secrets.gschema.xml.in:48
msgid "Path to the last opened database."
msgstr "Шлях да апошняй адкрытай базы даных."

#: data/org.gnome.World.Secrets.gschema.xml.in:52
msgid "Remember composite key"
msgstr "Запомніць кампазітны ключ"

#: data/org.gnome.World.Secrets.gschema.xml.in:53
msgid "Remember last used composite key for database unlocking."
msgstr ""
"Запомніць апошні скарыстаны кампазітны ключ для адблакавання базы даных"

#: data/org.gnome.World.Secrets.gschema.xml.in:57
msgid "Last used key provider"
msgstr ""

#: data/org.gnome.World.Secrets.gschema.xml.in:58
msgid "Key provider for composite database unlocking."
msgstr ""

#: data/org.gnome.World.Secrets.gschema.xml.in:62
msgid "Remember unlock method"
msgstr "Запомніць спосаб адблакавання"

#: data/org.gnome.World.Secrets.gschema.xml.in:63
msgid "Remember last used unlock method for future database unlocking."
msgstr ""

#: data/org.gnome.World.Secrets.gschema.xml.in:67
msgid "Backup the database on unlock"
msgstr ""

#: data/org.gnome.World.Secrets.gschema.xml.in:68
msgid ""
"If an error occurs while saving the database, a backup can be found at ~/."
"cache/secrets/backups"
msgstr ""

#: data/org.gnome.World.Secrets.gschema.xml.in:72
msgid "Sorting order of groups and entries"
msgstr ""

#: data/org.gnome.World.Secrets.gschema.xml.in:73
msgid "Order of the rows in the groups and entries view."
msgstr ""

#: data/org.gnome.World.Secrets.gschema.xml.in:77
msgid "Use uppercases when generating a password"
msgstr ""

#: data/org.gnome.World.Secrets.gschema.xml.in:78
msgid "Use uppercase characters A-Z when generating a random password."
msgstr ""

#: data/org.gnome.World.Secrets.gschema.xml.in:82
msgid "Use lowercases when generating a password"
msgstr ""

#: data/org.gnome.World.Secrets.gschema.xml.in:83
msgid "Use lowercase characters a-z when generating a random password."
msgstr ""

#: data/org.gnome.World.Secrets.gschema.xml.in:87
msgid "Use numbers when generating a password"
msgstr ""

#: data/org.gnome.World.Secrets.gschema.xml.in:88
msgid "Use numbers 0-9 characters when generating a random password."
msgstr ""

#: data/org.gnome.World.Secrets.gschema.xml.in:92
msgid "Use symbols when generating a password"
msgstr ""

#: data/org.gnome.World.Secrets.gschema.xml.in:93
msgid "Use non-alphanumeric ASCII symbols when generating a random password."
msgstr ""

#: data/org.gnome.World.Secrets.gschema.xml.in:97
msgid "Password length when generating a password"
msgstr ""

#: data/org.gnome.World.Secrets.gschema.xml.in:98
msgid "Number of single characters when generating a password."
msgstr ""

#: data/org.gnome.World.Secrets.gschema.xml.in:102
msgid "Number of words when generating a passphrase"
msgstr ""

#: data/org.gnome.World.Secrets.gschema.xml.in:107
msgid "Separator when generating a passphrase"
msgstr ""

#: data/org.gnome.World.Secrets.gschema.xml.in:108
msgid "Word separator to use when generating a passphrase."
msgstr ""

#: data/org.gnome.World.Secrets.metainfo.xml.in.in:5
msgid "@NAME@"
msgstr ""

#: data/org.gnome.World.Secrets.metainfo.xml.in.in:10
msgid "Manage your passwords"
msgstr ""

#: data/org.gnome.World.Secrets.metainfo.xml.in.in:12
msgid ""
"Secrets is a password manager which makes use of the KeePass v.4 format. It "
"integrates perfectly with the GNOME desktop and provides an easy and "
"uncluttered interface for the management of password databases."
msgstr ""

#: data/org.gnome.World.Secrets.metainfo.xml.in.in:357
msgid "Password management"
msgstr ""

#: data/org.gnome.World.Secrets.metainfo.xml.in.in:361
msgid "Safe unlocking"
msgstr ""

#: data/org.gnome.World.Secrets.metainfo.xml.in.in:365
msgid "Safe creation"
msgstr ""

#: gsecrets/application.py:39
msgid "Enable debug logging"
msgstr ""

#: gsecrets/attachment_warning_dialog.py:71
#: gsecrets/attachment_warning_dialog.py:82
msgid "Could not load attachment"
msgstr ""

#: gsecrets/create_database.py:65
msgid "Passwords do not match"
msgstr ""

#: gsecrets/create_database.py:80
msgid "Unable to create database"
msgstr ""

#: gsecrets/create_database.py:111 gsecrets/unlock_database.py:98
#: gsecrets/widgets/database_settings_dialog.py:208
msgid "Failed to generate composite key"
msgstr ""

#: gsecrets/entry_page.py:289
msgid "Enter tag name"
msgstr ""

#: gsecrets/entry_page.py:319
msgid "The tag already exists"
msgstr ""

#: gsecrets/entry_page.py:340 gsecrets/widgets/credentials_group.py:103
msgid "Saved in history"
msgstr ""

#: gsecrets/entry_page.py:359 gsecrets/entry_row.py:163
#: gsecrets/widgets/credentials_group.py:98
msgid "Username copied"
msgstr ""

#: gsecrets/entry_page.py:364 gsecrets/entry_page.py:497
msgid "Address copied"
msgstr "Адрас скапіяваны"

#: gsecrets/entry_page.py:373 gsecrets/entry_page.py:428
msgid "One-time password copied"
msgstr ""

#: gsecrets/entry_page.py:406
msgid "Could not open URL"
msgstr ""

#: gsecrets/entry_page.py:416
msgid "The address is not valid"
msgstr ""

#. NOTE: Filechooser title for selecting attachment file
#: gsecrets/entry_page.py:453
msgid "Select Attachments"
msgstr ""

#: gsecrets/entry_row.py:156 gsecrets/widgets/credentials_group.py:110
msgid "Password copied"
msgstr ""

#: gsecrets/entry_row.py:171
msgid "One-Time Password copied"
msgstr ""

#: gsecrets/entry_row.py:185 gsecrets/group_row.py:136
msgid "Title not Specified"
msgstr ""

#: gsecrets/password_generator_popover.py:74
msgid "Include Lowercase"
msgstr ""

#: gsecrets/password_generator_popover.py:76
msgid "Exclude Lowercase"
msgstr ""

#: gsecrets/password_generator_popover.py:79
msgid "Include Uppercase"
msgstr ""

#: gsecrets/password_generator_popover.py:81
msgid "Exclude Uppercase"
msgstr ""

#: gsecrets/password_generator_popover.py:84
msgid "Include Numbers"
msgstr ""

#: gsecrets/password_generator_popover.py:86
msgid "Exclude Numbers"
msgstr ""

#: gsecrets/password_generator_popover.py:89
msgid "Include Symbols"
msgstr ""

#: gsecrets/password_generator_popover.py:91
msgid "Exclude Symbols"
msgstr ""

#: gsecrets/provider/file_provider.py:34 gsecrets/provider/file_provider.py:131
#: gsecrets/provider/file_provider.py:197 gsecrets/utils.py:110
msgid "Keyfile"
msgstr ""

#: gsecrets/provider/file_provider.py:42 gsecrets/provider/file_provider.py:80
msgid "Select Keyfile"
msgstr ""

#: gsecrets/provider/file_provider.py:51 gsecrets/provider/file_provider.py:142
msgid "Clear Keyfile"
msgstr ""

#: gsecrets/provider/file_provider.py:70
msgid "Select keyfile"
msgstr ""

#: gsecrets/provider/file_provider.py:108
msgid "Could not load keyfile"
msgstr ""

#: gsecrets/provider/file_provider.py:135
#: gsecrets/provider/file_provider.py:195
msgid "Generate Keyfile"
msgstr ""

#: gsecrets/provider/file_provider.py:159
msgid "Use a file to increase security"
msgstr ""

#: gsecrets/provider/file_provider.py:181
msgid "Could not create keyfile"
msgstr ""

#: gsecrets/provider/pkcs11_provider.py:99
msgid "No Smartcard"
msgstr ""

#: gsecrets/provider/pkcs11_provider.py:107
msgid "No RSA Certificate"
msgstr ""

#: gsecrets/provider/pkcs11_provider.py:119
#: gsecrets/provider/pkcs11_provider.py:268
msgid "Smartcard"
msgstr ""

#: gsecrets/provider/pkcs11_provider.py:120
msgid "Select certificate"
msgstr ""

#: gsecrets/provider/pkcs11_provider.py:196
msgid "No smartcard present"
msgstr ""

#: gsecrets/provider/pkcs11_provider.py:197
msgid "Please insert smartcard and retry."
msgstr ""

#: gsecrets/provider/pkcs11_provider.py:200
#: gsecrets/widgets/database_settings_dialog.py:246
msgid "_OK"
msgstr ""

#: gsecrets/provider/pkcs11_provider.py:216
msgid "Unlock your smartcard"
msgstr ""

#: gsecrets/provider/pkcs11_provider.py:261
msgid "Failed to unlock Smartcard"
msgstr ""

#: gsecrets/provider/pkcs11_provider.py:269
msgid "Use a smartcard"
msgstr ""

#: gsecrets/provider/pkcs11_provider.py:323
msgid "Refresh Certificate List"
msgstr ""

#: gsecrets/provider/yubikey_provider.py:36
msgid "No Key"
msgstr ""

#. TRANSLATORS For example: YubiKey 4 [123456] - Slot 2
#: gsecrets/provider/yubikey_provider.py:39
#, python-brace-format
msgid "{description} [{serial}] - Slot {slot}"
msgstr ""

#: gsecrets/provider/yubikey_provider.py:143
#: gsecrets/provider/yubikey_provider.py:205
msgid "YubiKey"
msgstr ""

#: gsecrets/provider/yubikey_provider.py:144
msgid "Select key"
msgstr ""

#. TRANSLATORS YubiKey is a proper name key, see https://en.wikipedia.org/wiki/YubiKey.
#: gsecrets/provider/yubikey_provider.py:154
#: gsecrets/provider/yubikey_provider.py:213
msgid "Select YubiKey slot"
msgstr ""

#: gsecrets/provider/yubikey_provider.py:246
msgid "Touch YubiKey"
msgstr ""

#: gsecrets/recent_files_menu.py:29
msgid "Recent Files"
msgstr ""

#: gsecrets/safe_element.py:43
msgid "White"
msgstr ""

#: gsecrets/safe_element.py:45
msgid "Blue"
msgstr ""

#: gsecrets/safe_element.py:47
msgid "Green"
msgstr ""

#: gsecrets/safe_element.py:49
msgid "Yellow"
msgstr ""

#: gsecrets/safe_element.py:51
msgid "Orange"
msgstr ""

#: gsecrets/safe_element.py:53
msgid "Red"
msgstr ""

#: gsecrets/safe_element.py:55
msgid "Purple"
msgstr ""

#: gsecrets/safe_element.py:57
msgid "Brown"
msgstr ""

#: gsecrets/safe_element.py:656
msgid "Clone"
msgstr ""

#. This shouldn't happen
#. TODO Set the body of the message based on the error kind.
#: gsecrets/save_dialog.py:32 gsecrets/save_dialog.py:39
#: gsecrets/unlocked_database.py:248 gsecrets/unlocked_database.py:536
#: gsecrets/widgets/window.py:418
msgid "Could not save Safe"
msgstr ""

#: gsecrets/selection_manager.py:58
msgid "Operation aborted: deleting currently active group"
msgstr ""

#: gsecrets/selection_manager.py:106
msgid "Warning"
msgstr ""

#: gsecrets/selection_manager.py:108
msgid ""
"You are deleting elements in the trash bin, these deletions cannot be undone."
msgstr ""

#: gsecrets/selection_manager.py:142
msgid "Operation aborted: moving currently active group"
msgstr ""

#: gsecrets/selection_manager.py:155
msgid "Move completed"
msgstr ""

#. TRANSLATORS For example: "Safe ~/MySafe.kdbx is already open"
#: gsecrets/unlock_database.py:106
#, python-brace-format
msgid "Safe {path} is already open"
msgstr ""

#: gsecrets/unlock_database.py:187
msgid "Failed to unlock Safe"
msgstr ""

#: gsecrets/unlocked_database.py:251
msgid "Safe saved"
msgstr "Змены захаваны"

#: gsecrets/unlocked_database.py:253
msgid "No changes made"
msgstr ""

#. pylint: disable=consider-using-f-string
#: gsecrets/unlocked_database.py:301
msgid "Element deleted"
msgid_plural "{} Elements deleted"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""

#: gsecrets/unlocked_database.py:306
msgid "Deletion completed"
msgstr ""

#: gsecrets/unlocked_database.py:311 gsecrets/unlocked_database.py:335
msgid "Undo"
msgstr ""

#: gsecrets/unlocked_database.py:334
msgid "Attribute deleted"
msgstr ""

#: gsecrets/unlocked_database.py:363
msgid "Copied"
msgstr ""

#. NOTE: Notification that a safe has been locked.
#: gsecrets/unlocked_database.py:463
msgid "Safe locked due to inactivity"
msgstr ""

#. TRANSLATORS e.g. "21 Selected". What is selected is an 'Item'.
#: gsecrets/unlocked_database.py:701
msgid "{} Selected"
msgid_plural "{} Selected"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""

#: gsecrets/widgets/add_attribute_dialog.py:36
msgid "Attribute key contains an illegal character"
msgstr ""

#: gsecrets/widgets/add_attribute_dialog.py:46
msgid "Attribute key already exists"
msgstr ""

#. NOTE: Filechooser title for downloading an attachment
#: gsecrets/widgets/attachment_entry_row.py:37
msgid "Save Attachment"
msgstr ""

#: gsecrets/widgets/attachment_entry_row.py:54
msgid "Could not store attachment"
msgstr ""

#: gsecrets/widgets/attribute_entry_row.py:43
#: gsecrets/widgets/protected_attribute_entry_row.py:60
msgid "Attribute copied"
msgstr ""

#: gsecrets/widgets/database_settings_dialog.py:175
#: gsecrets/widgets/database_settings_dialog.py:179
msgid "Could not apply changes"
msgstr ""

#. Still executes the finally block
#: gsecrets/widgets/database_settings_dialog.py:182
msgid "Changes Applied"
msgstr ""

#: gsecrets/widgets/database_settings_dialog.py:236
msgid "Could not change credentials"
msgstr ""

#. TRANSLATORS Warning dialog to resolve file saving conflicts.
#: gsecrets/widgets/database_settings_dialog.py:241
#: gsecrets/widgets/saving_conflict_dialog.py:22
msgid "Conflicts While Saving"
msgstr ""

#: gsecrets/widgets/database_settings_dialog.py:243
msgid ""
"The safe was modified from somewhere else. Please resolve these conflicts "
"from the main window when saving."
msgstr ""

#. Encryption Algorithm
#. Derivation Algorithm
#: gsecrets/widgets/database_settings_dialog.py:316
#: gsecrets/widgets/database_settings_dialog.py:330
msgid "Unknown"
msgstr ""

#. NOTE: AES is a proper name
#: gsecrets/widgets/database_settings_dialog.py:320
msgid "AES 256-bit"
msgstr ""

#. NOTE: ChaCha20 is a proper name
#: gsecrets/widgets/database_settings_dialog.py:323
msgid "ChaCha20 256-bit"
msgstr ""

#. NOTE: Twofish is a proper name
#: gsecrets/widgets/database_settings_dialog.py:326
msgid "Twofish 256-bit"
msgstr ""

#. NOTE: Argon2 is a proper name
#: gsecrets/widgets/database_settings_dialog.py:334
msgid "Argon2"
msgstr ""

#. NOTE: Argon2id is a proper name
#: gsecrets/widgets/database_settings_dialog.py:337
msgid "Argon2id"
msgstr ""

#. NOTE: AES-KDF is a proper name
#: gsecrets/widgets/database_settings_dialog.py:340
msgid "AES-KDF"
msgstr ""

#: gsecrets/widgets/expiration_date_row.py:56
#: gsecrets/widgets/expiration_date_row.py:64
msgid "Entry expired"
msgstr ""

#: gsecrets/widgets/history_row.py:43
msgid "Hide Text"
msgstr ""

#: gsecrets/widgets/notes_dialog.py:105
msgid "Selection copied"
msgstr ""

#: gsecrets/widgets/password_level_bar.py:22
msgid "Password Strength"
msgstr ""

#. TRANSLATORS Dialog header for when there is a conflict.
#: gsecrets/widgets/quit_conflict_dialog.py:18
msgid "Unsaved Changes Conflict"
msgstr ""

#: gsecrets/widgets/quit_conflict_dialog.py:20
msgid "Save and Quit"
msgstr "Выйсці і захаваць"

#: gsecrets/widgets/quit_conflict_dialog.py:21
msgid "Back up, Save, and Quit"
msgstr ""

#: gsecrets/widgets/quit_conflict_dialog.py:22
msgid "Don't Quit"
msgstr "Не выходзіць"

#. TRANSLATORS Warning Dialog to resolve saving conflicts. \n is a new line.
#: gsecrets/widgets/saving_conflict_dialog.py:25
msgid ""
"The safe was modified from somewhere else. Saving will overwrite their "
"version of the safe with our current version.\n"
"\n"
" You can also make a backup of their version of the safe."
msgstr ""

#. TRANSLATORS backup and save current safe.
#: gsecrets/widgets/saving_conflict_dialog.py:33
msgid "_Back up and Save"
msgstr ""

#: gsecrets/widgets/saving_conflict_dialog.py:45
msgid "Save Backup"
msgstr ""

#: gsecrets/widgets/saving_conflict_dialog.py:55
msgid "Could not backup safe"
msgstr ""

#: gsecrets/widgets/window.py:151
msgid "Any file type"
msgstr ""

#: gsecrets/widgets/window.py:158
msgid "Select Safe"
msgstr ""

#: gsecrets/widgets/window.py:175
msgid "File does not have a valid path"
msgstr ""

#: gsecrets/widgets/window.py:208
msgid "Safe is already open"
msgstr ""

#: gsecrets/widgets/window.py:240
msgid "_Create"
msgstr ""

#: gsecrets/widgets/window.py:262
msgid "Cannot create Safe: Safe is already open"
msgstr ""

#: gsecrets/widgets/window.py:305 gsecrets/widgets/window.py:318
msgid "Could not create new Safe"
msgstr ""

#: gsecrets/widgets/window.py:395
msgid "Could not save safe"
msgstr ""

#. TRANSLATORS Add your name to the translator credits list
#: gsecrets/widgets/window.py:561
msgid "translator-credits"
msgstr ""
